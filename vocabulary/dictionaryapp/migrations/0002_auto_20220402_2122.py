# Generated by Django 3.2 on 2022-04-02 21:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaryapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='word',
            name='example',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='word',
            name='note',
            field=models.TextField(blank=True),
        ),
    ]
