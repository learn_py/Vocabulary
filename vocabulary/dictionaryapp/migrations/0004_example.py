# Generated by Django 3.2 on 2022-04-03 22:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dictionaryapp', '0003_remove_word_example'),
    ]

    operations = [
        migrations.CreateModel(
            name='Example',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('eng_example', models.CharField(max_length=256)),
                ('rus_example', models.CharField(max_length=256)),
                ('word', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='dictionaryapp.word')),
            ],
        ),
    ]
